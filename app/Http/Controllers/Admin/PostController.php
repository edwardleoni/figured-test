<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Post;
use Redirect;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showIndex()
    {
        $posts = Post::all()->sortByDesc("_id");
        return view('admin/index', [
            "posts"=> $posts
        ]);
    }

    public function showAdd()
    {
        return view('admin/add');
    }

    public function doAdd(Request $request)
    {
        $title = $request->input("title");
        $content = $request->input("content");

        $validator = self::validation([
            "title" => $title,
            "content" => $content 
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors())->withInput();
        }

        // Only adds if valid
        Post::create([
            "title" => $title,
            "content" => $content
        ]);

        return Redirect(route("admin"));
    }

    public function showEdit(Request $request, $postId)
    {
        $post = Post::where("_id", $postId);
        
        // quick check to see if it exists
        if ($post->count() == 0) {
            redirect(route("admin"));
        }

        $post = $post->get()[0];
       
        return view('admin/edit', [
            "post" => $post
        ]);
    }

    public function doEdit(Request $request, $postId)
    {
        $title = $request->input("title");
        $content = $request->input("content");
        
        $validator = self::validation([
            "title" => $title,
            "content" => $content 
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors())->withInput();
        }

        // Only edits if valid
        $post = Post::find($postId);
        $post->title = $title;
        $post->content = $content;
        $post->save();

        return Redirect(route("admin"));
    }

    public function doRemove(Request $request, $postId)
    {
        Post::destroy($postId);
        return Redirect(route("admin"));
    }

    private function validation($data) 
    {
        return Validator::make($data, [
            'title' => 'required|max:255',
            'content' => 'required|min:5',
        ]);
    }
}
