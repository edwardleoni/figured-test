<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Post;
use Redirect;

class FrontendController extends Controller
{
   
    public function showIndex()
    {
        $posts = Post::all()->sortByDesc("_id");
        return view('index', [
            "posts"=> $posts
        ]);
    }

    public function showPost(Request $request, $postId)
    {
        $post = Post::where("_id", $postId);

        // quick id validation
        if ($post->count() == 0) {
            return Redirect("/");
        }

        return view('post', [
            "post"=> $post->get()[0]
        ]);
    }

}
