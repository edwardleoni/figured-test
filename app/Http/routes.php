<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication Routes...
$this->get('login', 'Auth\AuthController@showLoginForm');
$this->post('login', 'Auth\AuthController@login');
$this->get('logout', 'Auth\AuthController@logout');

// Registration Routes...
//$this->get('register', 'Auth\AuthController@showRegistrationForm');
//$this->post('register', 'Auth\AuthController@register');

// Password Reset Routes...
$this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
$this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
$this->post('password/reset', 'Auth\PasswordController@reset');

// Admin Routes
Route::get('/admin', ['as' => 'admin', 'uses' => 'Admin\PostController@showIndex']);
Route::get('/admin/add', ['as' => 'admin.add', 'uses' => 'Admin\PostController@showAdd']);
Route::post('/admin/add', ['as' => 'admin.add', 'uses' => 'Admin\PostController@doAdd']);
Route::get('/admin/{postId}/edit', ['as' => 'admin.edit', 'uses' => 'Admin\PostController@showEdit']);
Route::post('/admin/{postId}/edit', ['as' => 'admin.edit', 'uses' => 'Admin\PostController@doEdit']);
Route::get('/admin/{postId}/remove', ['as' => 'admin.remove', 'uses' => 'Admin\PostController@doRemove']);

// Public routes
Route::get('/', ['as' => '', 'uses' => 'FrontendController@showIndex']);
Route::get('/{postId}', ['as' => 'post', 'uses' => 'FrontendController@showPost']);