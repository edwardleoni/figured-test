# The task
"I would like you to build a very simple blog application where users can read posts and where an admin user can login and create, update and delete posts.
If you could use the following as part of the project:
Laravel 5.2
PHP7
MongoDB for storing the blog posts - OK to use MySQL for users as this comes standard in Laravel.
Can use whatever you like for the front end, plain Laravel blade is fine."

# Project Already setup online
- Go to http://figured.eduardoleoni.com.br/
- To login on the dashboard go to http://figured.eduardoleoni.com.br/admin
- Use the pre-set credentials( Email: c.eduardo.leoni@gmail.com, Pass: F1guR3d!!!!! ) 
- I setup the above url on a VPS I configured myself using my own name server, I get extra points for that, right? :P

# Project Installation Requirements
- Composer
- MySQL/MariaDB Server
- MongoDB Server
- Apache/NginX
- PHP5+

# Project installation
- Clone this repo
- Composer install
- Copy the .env-sample and name it as .env, fill up important data such as database details
- php key:generate
- php artisan migrate 
- Uncomment the Registration Routes so you can register your user to get access to the dashboard
- Register a user and then login