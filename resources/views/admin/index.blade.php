@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            
              <a href="{{ route('admin.add') }}" class="col-md-3 col-md-offset-9">
                <input type="button" class="form-control" value="Add new">
              </a>

              <div class="panel-heading">Posts</div>

              <table class="table">
                <tr>
                    <th>Title</th>
                    <th>Actions</th>
                </tr>
                @foreach($posts as $post)
                 <tr>
                    <td class="col-md-9">{{ $post->title }}</td>
                    <td class="col-md-3 bs-glyphicons">
                        <a href="{{ route('admin.edit', $post->_id) }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> <span class="glyphicon-class">Edit</span></a> | 
                        <a href="{{ route('admin.remove', $post->_id) }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <span class="glyphicon-class">Remove</span></a> 
                        
                    </td>
                  </tr>
                @endforeach
              </table>

            
        </div>
    </div>
</div>
@endsection
