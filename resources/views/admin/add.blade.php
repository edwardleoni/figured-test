@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            
              <h1>Add new post</h1>

                  <form class="" role="form" method="POST" action="{{ route('admin.add') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            

                            <div class="col-md-12">
                                <input id="title" type="text" class="form-control" placeholder="Post title goes here" name="title" value="{{ old('title') }}">

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                           
                            <div class="col-md-12">
                                <textarea id="content" placeholder="Post content goes here" style="height: 300px" class="form-control" name="content">{{ old('content') }}</textarea>

                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                           
                        <button type="submit" class="btn btn-primary" style="float: right; margin-top: 20px;">
                                     Post!
                        </button>

                               
                          
                        
                    </form>

            
        </div>
    </div>
</div>
@endsection
