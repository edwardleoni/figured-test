@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                @if(count($posts) < 1)
                    <div class="panel-heading"><p>No posts yet</p></div>
                @else
                    @foreach($posts as $post)
                        <div class="panel-heading">{{ $post->title }}</div>

                        <div class="panel-body">
                            {{ substr($post->content, 0, 250) }}...
                        </div>
                        <div class="panel-body">
                            <a href="{{ route('post', $post->_id) }}">Read more</a>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
